### Auteurs

* **Jessy Provencher** - [jessyprovencher](https://gitlab.com/jessyprovencher)
* **Philip Laliberté-Duguay** - [Philipou120](https://gitlab.com/Philipou120)
* **Patrick Boucher** - [h7teen](https://gitlab.com/h7teen)
* **Alexandre Fontaine** - [Aouk15](https://gitlab.com/Aouk15)

## Configurations initiales

### Prérequis

*  Avoir installé PHP et Composer.<br>
    * Windows → Voir les liens de téléchargements suivants: [PHP](https://www.php.net/downloads.php) & [Composer](https://getcomposer.org/download/)
    * Ubuntu → En utilisant les commandes suivantes ou voir les documentations: [PHP](https://doc.ubuntu-fr.org/php) & [Composer](https://doc.ubuntu-fr.org/composer)
        * `sudo apt update && sudo apt install wget php-cli php-zip unzip`
        * `php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"`
        * `sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer`
*  Avoir un serveur de base de données supporté par Laravel:
    *  MySQL/MariaDB (Recommandé)
    *  PostgreSQL
    *  SQLite
    *  SQL Server

### Installation
0. Si Laravel n'est pas encore installé, lancer la commande:
`composer global require “laravel/installer”`
1.  Créer la base de données "laravel" dans le serveur (utiliser les caractères UTF-8).
2.  Cloner le dépôt git grâce au lien suivant:
    https://gitlab.com/Philipou120/presentation_st.git
3.  Ouvrir l'invite de commande ou autre terminal et naviguer vers le dossier du projet.
4.  Lancer les commandes suivantes :
`composer install`
`php artisan key:generate`
5.  Lancer les migrations et peupler la base de données :
`php artisan migrate:fresh --seed`
6.  Lancer le serveur de développement :
`php artisan serve`