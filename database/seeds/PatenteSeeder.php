<?php

use Illuminate\Database\Seeder;

class PatenteSeeder extends Seeder
{
    public function run()
    {
        DB::table('patentes')->delete();
        for ($i = 0; $i < 10; $i++) {
            \App\Patente::create();
        }
    }
}
