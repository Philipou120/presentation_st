
<div class="menu_selection" id="menu_selection" style="background: lightgray">
    <div class="input-group mt-4">
        <a href="create" class="btn btn-default">
            <i class="fa fa-plus-square" aria-hidden="true"></i>
        </a>
        <a href="#" class="btn btn-default">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </a>
        <a href="#" class="btn btn-default">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
    </div>
</div>

<div class="container col-md-12 col-md-offset-2 mt-3">
    <div class="card">
        <div class="card-header ">
            <h5 class="float-left">Patentes</h5>
            <div class="clearfix"></div>
        </div>
        <div class="card-body mt-2">
            @if (!$patentes->isEmpty())
                <table class="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Est une patente à gosse ?</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($patentes as $patente)
                            <tr>
                                <td>{{$patente->id}}</td>
                                <td>{{$patente->patente_a_gosse == true ? 'Oui' : 'Non'}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>