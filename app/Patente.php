<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patente extends Model
{
    public $timestamps = false;

    protected $fillable = ['patente_a_gosse'];
}
