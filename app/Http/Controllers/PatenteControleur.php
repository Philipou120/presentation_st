<?php

namespace App\Http\Controllers;

use App\Patente;
use Illuminate\Http\Request;

class PatenteControleur extends Controller
{
    public static function getPatentes()
    {
        $patentes = Patente::all();
        return $patentes;
    }

    public function store()
    {
        Patente::create();
        return redirect()->back();
    }

    public function update($id)
    {
        $patente = Patente::find($id);
        if ($patente != null) {
            $patente->patente_a_gosse = $patente->patente_a_gosse == 1 ? 0 : 1;
            $patente->save();
        }
        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        Patente::find($request->id)->delete();
        return redirect()->back();
    }
}
